﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_AdminLayer_DodajPrzedmiot : System.Web.UI.Page
{
    private static string path = HttpContext.Current.Server.MapPath("~/App_Data/DziekanatDB.mdf");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }


    protected void DodajPrzedmiot(string nazwa, string wyk_l_godzin, string lab_l_godzin, bool egzamin, string rodzaj)
    {
        try
        {
            if (nazwa.Length==0||wyk_l_godzin.Length==0||lab_l_godzin.Length==0||rodzaj.Length==0)
            {
                throw new Exception();
            }
            String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.Text;
            command.CommandText = String.Format("insert into Przedmioty (nazwa, wyk_l_godzin, lab_l_godzin, egzamin, rodzaj) values('{0}','{1}','{2}','{3}','{4}')", nazwa, Convert.ToInt32(wyk_l_godzin), Convert.ToInt32(lab_l_godzin), egzamin, rodzaj);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            Response.Write("<script>alert('Dodano przedmiot "+nazwa+"');</script>");
        }
        catch(Exception e)
        {

            Response.Write("<script>alert('Bład dodawania rekordu');</script>");
        }

    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        string nazwa = TextBox_nazwa.Text;
        string wyk_l_godzin = TextBox_wyk_l_godzin.Text;
        string lab_l_godzin = TextBox_lab_l_godzin.Text;
        bool egzamin = CheckBox_egzamin.Checked;
        string rodzaj = TextBox_rodzaj.Text;
        DodajPrzedmiot(nazwa, wyk_l_godzin, lab_l_godzin, egzamin, rodzaj);
        //Server.Transfer("Przedmioty.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Server.Transfer("Przedmioty.aspx");
    }
}